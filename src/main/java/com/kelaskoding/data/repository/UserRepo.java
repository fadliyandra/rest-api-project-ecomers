package com.kelaskoding.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.kelaskoding.data.entities.User;



public interface UserRepo extends JpaRepository<User, Long>{
    
    public User findByEmailAndPassword(String email, String password);

    public User findByEmail(String email);

    public boolean existsByEmail(String email);


    
}
