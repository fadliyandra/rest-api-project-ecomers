package com.kelaskoding.data.repository;

import org.springframework.data.repository.CrudRepository;

import com.kelaskoding.data.entities.Session;

public interface SessionRepo extends CrudRepository<Session, Long>{

    public Session findBySessionId(String sessionId);
    
    
}
