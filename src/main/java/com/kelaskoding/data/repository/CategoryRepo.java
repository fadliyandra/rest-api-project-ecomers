package com.kelaskoding.data.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.kelaskoding.data.entities.Category;

@Repository
public interface CategoryRepo extends CrudRepository<Category, Long>{

    public Category findByName(String name); //drived query method

   //use jpql
   @Query("SELECT c FROM Category c WHERE c.name = :paramName")
   public Category cariBerdasarkanNama(@Param("paramName") String name);

   public List<Category> findByNameContaining(String name);

   public List<Category> findByDeleted(boolean deleted);







}
 