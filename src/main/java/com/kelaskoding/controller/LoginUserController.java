package com.kelaskoding.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kelaskoding.dto.RequestLogin;
import com.kelaskoding.dto.ResponseData;
import com.kelaskoding.service.UserService;

@RestController
@RequestMapping("/api/v1/users/login")
public class LoginUserController {

    private final UserService userService;

    public LoginUserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping
    public ResponseEntity<?> login(@RequestBody RequestLogin login){
        ResponseData responseData = new ResponseData();
        try {
            responseData.setPayload(userService.login(login.getEmail(), login.getPassword()));
            responseData.setStatus(true);
            responseData.getMessage().add("Login succesfull");
            return ResponseEntity.ok(responseData);

        } catch (Exception e) {
            responseData.setStatus(false);
            responseData.getMessage().add("Login failed: " + e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(responseData);
            // TODO: handle exception
        }
    }
    
}
