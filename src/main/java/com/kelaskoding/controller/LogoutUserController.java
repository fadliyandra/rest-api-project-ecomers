package com.kelaskoding.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kelaskoding.dto.ResponseData;
import com.kelaskoding.service.UserService;

import jakarta.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/v1/user/logout")
public class LogoutUserController {

    private final UserService userService;

    public LogoutUserController(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    private HttpServletRequest request;


    @GetMapping
    public ResponseEntity<?> userLogout(){
        ResponseData responseData = new ResponseData();

        try {
            String sessioId = request.getHeader("x-session-id");
            userService.logout(sessioId);
            responseData.setStatus(true);
            responseData.getMessage().add("Logout succesfull");
            return ResponseEntity.ok(responseData);
        } catch (Exception e) {
            // TODO: handle exception
            responseData.setStatus(false);
            responseData.getMessage().add("logout failed " + e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(responseData);
            
        }

    }
    
}
