package com.kelaskoding.controller;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kelaskoding.data.entities.User;
import com.kelaskoding.dto.RequestRegister;
import com.kelaskoding.dto.ResponseData;
import com.kelaskoding.dto.ResponseRegister;
import com.kelaskoding.service.UserService;

import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/v1/users/register")
public class RegisterUserController {

    @Autowired
    private UserService userService;

    @Autowired
    private ModelMapper modelMapper;

    @PostMapping
    public ResponseEntity<ResponseData<ResponseRegister>> registerUser(@Valid @RequestBody RequestRegister requestRegister, Errors errors){

        ResponseData<ResponseRegister> responseData = new ResponseData<>();

        if (errors.hasErrors()) {
            errors.getAllErrors().forEach(error ->{
                responseData.getMessage().add(error.getDefaultMessage());
            });
            responseData.setStatus(false);
            return ResponseEntity.badRequest().body(responseData);

        }

        try {
            
            // User user = new User();
            // user.setEmail(requestRegister.getEmail());
            // user.setPassword(requestRegister.getPassword());
            // user.setFirstName(requestRegister.getFirstName());
            // user.setLastName(requestRegister.getLastName());
            // user.setAge(requestRegister.getAge());
            User user = modelMapper.map(requestRegister, User.class);
            
            userService.addUser(user);

            responseData.setStatus(true);
            responseData.getMessage().add("User Registered successfully");
            responseData.setPayload(modelMapper.map(user, ResponseRegister.class));
            return ResponseEntity.ok(responseData);
            
        } catch (Exception e) {

            responseData.setStatus(false);
            responseData.getMessage().add("Error" + e.getMessage());
            return ResponseEntity.status(500).body(responseData);
        }

  
    }
}
