package com.kelaskoding.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kelaskoding.data.entities.Product;
import com.kelaskoding.dto.RequestProduct;
import com.kelaskoding.dto.ResponseData;
import com.kelaskoding.service.CategoryService;
import com.kelaskoding.service.ProductService;

import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/v1/products")
public class ProductController {

    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @Autowired
    private CategoryService categoryService;

    @PostMapping
    public ResponseEntity<?> createdProduct(@Valid @RequestBody RequestProduct request, Errors errors){
        ResponseData responseData = new ResponseData();
        
        if (errors.hasErrors()) {
            errors.getAllErrors().forEach(error -> responseData.getMessage().add(error.getDefaultMessage()));
            responseData.setStatus(false);
            return ResponseEntity.badRequest().body(responseData);
            
        }

        try {
            Product product = new Product();
            product.setCode(request.getCode());
            product.setName(request.getName());
            product.setPrice(request.getPrice());
            product.setDescription(request.getDescription());
            product.setCategory(categoryService.findCategoryById(request.getCategoryId()));

            responseData.setPayload(productService.addProduct(product));
            responseData.setStatus(true);
            return ResponseEntity.ok(responseData);
        } catch (Exception e) {
            responseData.getMessage().add(e.getMessage());
            responseData.setStatus(false);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(responseData);
            // TODO: handle exception
        }
    }

    @GetMapping("/{size}/{page}")
    public ResponseEntity<?> getAllProduct(@PathVariable("size") int size, @PathVariable("page") int page){
        ResponseData responseData = new ResponseData();
        try {
            responseData.setPayload(productService.findAllProducts(size, page));
            responseData.setStatus(true);
            return ResponseEntity.ok(responseData);
        } catch (Exception e) {
            responseData.getMessage().add(e.getMessage());
            responseData.setStatus(false);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(responseData);
            // TODO: handle exception
        }
    }

    @GetMapping("/search/category/{id}")
    public ResponseEntity<?> getPorductByCatgeory(@PathVariable("id") long categoryId){
        ResponseData responseData = new ResponseData();
        try {
            responseData.setPayload(productService.searchProductByCategory(categoryId));
            responseData.setStatus(true);
            return ResponseEntity.ok(responseData);
        } catch (Exception e) {
            // TODO: handle exception
            responseData.getMessage().add(e.getMessage());
            responseData.setStatus(false);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(responseData);
        }
    }

}
