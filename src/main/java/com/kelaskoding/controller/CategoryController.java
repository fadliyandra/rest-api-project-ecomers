package com.kelaskoding.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kelaskoding.data.entities.Category;
import com.kelaskoding.dto.RequestSearchCategory;
import com.kelaskoding.dto.ResponseData;
import com.kelaskoding.service.CategoryService;

import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/v1/categories")
public class CategoryController {


    @Autowired
    private CategoryService categoryService;

    

        @GetMapping("/deleted/{isHapus}")
         public  ResponseEntity<ResponseData<Iterable<Category>>> findAllCategories(@PathVariable("isHapus") boolean isHapus){
        ResponseData<Iterable<Category>> responseData = new ResponseData<>();
        try {
            responseData.setPayload(categoryService.findAllCategories(isHapus));
            responseData.setStatus(true);
            return ResponseEntity.ok(responseData);

        } catch (Exception e) {
            // TODO: handle exception
            responseData.getMessage().add(e.getMessage());
            responseData.setStatus(false);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(responseData);
        }
    }

    // @GetMapping("/{id}")
    // public  Category finCategoryById(@PathVariable("id") long id){
    //     return categoryService.findCategoryById(id);
    // }

    //move to service
    @GetMapping("/{id}")
    public ResponseEntity<ResponseData<Category>> finCategoryById(@PathVariable("id") long id){
        ResponseData<Category> responseData = new ResponseData<>();

        try {
            Category category = categoryService.findCategoryById(id);
            if (category==null) {
                responseData.getMessage().add("category not found");
                responseData.setStatus(false);
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(responseData);
                
            }
            responseData.setPayload(category);
            responseData.setStatus(true);
            return ResponseEntity.ok(responseData);
        } catch (Exception e) {
            // TODO: handle exception
            responseData.getMessage().add(e.getMessage());
            responseData.setStatus(false);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(responseData);

        }


    }

    

    // @DeleteMapping("/{id}")
    // public void removeCategoryById(@PathVariable("id")long id){
    //     categoryService.removeCategoryById(id);
    // }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> removeCategoryById(@PathVariable("id")long id){
       ResponseData responseData = new ResponseData();
       try {
        boolean result = categoryService.removeCategoryById(id);
        if (!result) {
            responseData.getMessage().add("Category not found");
            responseData.setStatus(result);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(responseData);
            
        }
         responseData.setPayload(result);
         responseData.setStatus(result);
         return ResponseEntity.ok(responseData);
       } catch (Exception e) {
        // TODO: handle exception
            responseData.getMessage().add(e.getMessage());
            responseData.setStatus(false);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(responseData);

       }

    }


    

    // @PostMapping
    // public Category savCategory(@RequestBody Category category){
    //     return categoryService.addCatgeory(category);
    // }

    //move to service
    @PostMapping
    public ResponseEntity<?> savCategory(@Valid @RequestBody Category category, Errors errors){
        ResponseData responseData = new ResponseData();

        if (errors.hasErrors()) {
            errors.getAllErrors().forEach(e -> responseData.getMessage().add(e.getDefaultMessage()));
            responseData.setStatus(false);
            return ResponseEntity.badRequest().body(responseData);
            
        }
        try {
            responseData.setPayload(categoryService.addCatgeory(category));
            responseData.setStatus(true);
            return ResponseEntity.ok(responseData);

        } catch (Exception e) {
            // TODO: handle exception
            responseData.getMessage().add(e.getMessage());
            responseData.setStatus(false);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(responseData);
        }
    }

    // @PutMapping
    // public Category updCategory(@RequestBody Category category){
    //     return categoryService.updateCategory(category);
    // }

    @PutMapping
    public ResponseEntity<?> updCategory(@RequestBody Category category){
        ResponseData responseData = new ResponseData();
        try {
            responseData.setPayload(categoryService.updateCategory(category));
            responseData.setStatus(true);
            return ResponseEntity.ok(responseData);

        } catch (Exception e) {
            // TODO: handle exception
            responseData.getMessage().add(e.getMessage());
            responseData.setStatus(false);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(responseData);
        }
        
    }


        @PostMapping("/search")
        public ResponseEntity<?> findCategoryByName( @RequestBody RequestSearchCategory request){
            ResponseData responseData = new ResponseData();

            try {
                responseData.setPayload(categoryService.findByName(request.getSearchKey()));
                responseData.setStatus(true);
                return ResponseEntity.ok(responseData);

            } catch (Exception e) {
                // TODO: handle exception
                responseData.getMessage().add(e.getMessage());
                responseData.setStatus(false);
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(responseData);
            }

        }

        @PostMapping("/search/like")
        public ResponseEntity<?> findCategoryByNameContaining( @RequestBody RequestSearchCategory request){
            ResponseData responseData = new ResponseData();

            try {
                responseData.setPayload(categoryService.findByNameContaining(request.getSearchKey()));
                responseData.setStatus(true);
                return ResponseEntity.ok(responseData);

            } catch (Exception e) {
                // TODO: handle exception
                responseData.getMessage().add(e.getMessage());
                responseData.setStatus(false);
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(responseData);
            }

        }
    



}
