package com.kelaskoding.controller;

import java.util.HashMap;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/")
public class HelloController {

    @GetMapping("hello")
    public Object sayHello(){
        HashMap<String, String> map = new HashMap<>();
        map.put("mesage", "hello , World");
        map.put("status", "succes");
        return map;
    }
    
}
