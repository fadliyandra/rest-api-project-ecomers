package com.kelaskoding.validator;

import org.springframework.beans.factory.annotation.Autowired;

import com.kelaskoding.data.repository.UserRepo;
import com.kelaskoding.service.UserService;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class UniqueEmailValidator implements ConstraintValidator<UniqueEmailConstraint, String>{

    @Autowired
    private UserRepo userRepo;


    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return !userRepo.existsByEmail(value);
        
    }

    
}
