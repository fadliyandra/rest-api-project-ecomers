package com.kelaskoding.validator;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import jakarta.validation.Constraint;

@Target({ElementType.FIELD, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = UniqueCodeValidator.class)
public @interface UniqueCodeConstraint {

    String message() default "Code is Already Register";

    Class<?>[] groups() default {};

    Class<?>[] payload() default {};
}
