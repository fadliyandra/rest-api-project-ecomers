package com.kelaskoding.validator;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import jakarta.validation.Constraint;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = PasswordEqualValidator.class)
public @interface PasswordEqualsConstrain {

    String message() default "Password and RetypePassword must equals";

    Class<?>[] groups() default {};

    Class<?>[] payload() default {};
    
}
