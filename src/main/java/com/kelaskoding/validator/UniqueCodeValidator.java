package com.kelaskoding.validator;

import org.springframework.beans.factory.annotation.Autowired;

import com.kelaskoding.data.repository.ProductRepo;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class UniqueCodeValidator implements ConstraintValidator<UniqueCodeConstraint, String>{


    @Autowired
    private ProductRepo productRepo;

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        // TODO Auto-generated method stub
        return !productRepo.existsByCode(value);



    }

    
}
