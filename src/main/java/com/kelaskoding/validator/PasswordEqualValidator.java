package com.kelaskoding.validator;

import com.kelaskoding.dto.RequestRegister;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class PasswordEqualValidator implements ConstraintValidator<PasswordEqualsConstrain, Object>{

    @Override
    public boolean isValid(Object data, ConstraintValidatorContext context) {
        // TODO Auto-generated method stub

        RequestRegister requestRegister = (RequestRegister) data;
        return  requestRegister.getPassword().equals(requestRegister.getRetypePassword());
        
        
    }


    
}
