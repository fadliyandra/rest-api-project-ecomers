package com.kelaskoding.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.kelaskoding.data.entities.Category;
import com.kelaskoding.data.entities.Product;
import com.kelaskoding.data.repository.CategoryRepo;
import com.kelaskoding.data.repository.ProductRepo;

import jakarta.transaction.Transactional;

@Service
@Transactional
public class ProductService {

    @Autowired
    private ProductRepo repo;

    @Autowired
    private CategoryRepo categoryRepo;




     public Iterable<Product> findAllProducts(int size, int page) {
         Pageable pageable = PageRequest.of(page -1, size);
        return repo.findAll(pageable);
    }

    public Product findProductById(long id) {
        return repo.findById(id).orElse(null);
    }

    public void removeProductById(long id) {
        repo.deleteById(id);
    }

    public Product addProduct(Product product) {
        return repo.save(product);
    }

    public Product updateProduct(Product product) {
        return repo.save(product);
    }

    public List<Product> searchPrductByName(String name){
        return repo.findByNameContaining(name);
    }

    public List<Product> searchProductByCategory(long categoryId){
        Category category = categoryRepo.findById(categoryId).orElse(null);
        if (category == null) {
            return new ArrayList<>();
            
        }
        return repo.findByCategory(category);
    }

    public List<Product> searchProductByCategoryId(long categoryId){
        return repo.findByCategoryId(categoryId);
    }
    
}
