package com.kelaskoding.service;



import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kelaskoding.data.entities.Session;
import com.kelaskoding.data.entities.User;
import com.kelaskoding.data.repository.SessionRepo;
import com.kelaskoding.data.repository.UserRepo;

import jakarta.transaction.Transactional;

@Service
@Transactional
public class UserService {

    @Autowired
    private UserRepo personRepoJpa;

    @Autowired
    private SessionRepo sessionRepo;

    public Iterable<User> findAllUsers(){
        return personRepoJpa.findAll();
    }

    public User findUserById(long id) {
        return personRepoJpa.findById(id).orElse(null);
    }

    public User findUserByEmail(String email){
        return personRepoJpa.findByEmail(email);
    }

    public void removeUserById(long id){
        personRepoJpa.deleteById(id);
    }

    public void addUser(User user){
        personRepoJpa.save(user);
    }
    public void updateUser(User user){
        personRepoJpa.save(user);
    }
    public User findUserByEmailAndPassword(String email, String password){
        return personRepoJpa.findByEmailAndPassword(email, password);
    }

    public  Session login(String email, String password){
        User user = findUserByEmailAndPassword(email, password);
        if (user == null) {
            return null;
            
        }

        try {
            Session session = new Session();
            session.setUser(user);
            session.setActive(true);
            session.setSessionId(System.currentTimeMillis() + "" + user.getId());
            sessionRepo.save(session);
            return session;

        } catch (Exception e) {
            // TODO: handle exception
            return null;
        }
    }

    public boolean logout(String sessionId){
        Session session = sessionRepo.findBySessionId(sessionId);
        if (session == null) {
            return false;
        }
        session.setActive(false);
        session.setLogOutAt(new Date());
        sessionRepo.save(session);
        return true;
    }
}
