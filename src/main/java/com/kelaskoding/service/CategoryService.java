package com.kelaskoding.service;

import java.util.List;

import org.hibernate.Filter;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import com.kelaskoding.data.entities.Category;
import com.kelaskoding.data.repository.CategoryRepo;

import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;


@Service
@Transactional
public class CategoryService {

    @Autowired
    private CategoryRepo repo;

    @Autowired
    private EntityManager entityManager;

    public Iterable<Category> findAllCategories(boolean isHapus){
        // Session session = entityManager.unwrap(Session.class);  //to enable filter
        // Filter filter = session.enableFilter("deletedCategoryFilter");
        // filter.setParameter("isDeleted", isHapus);
        // Iterable<Category> categories = repo.findAll();
        // session.disableFilter("deletedCategoryFilter");
        return repo.findByDeleted(isHapus);
        
    }



    public Category findCategoryById(long id){
        return repo.findById(id).orElse(null);
    }

    public boolean removeCategoryById(long id){
       try {
        Category category = findCategoryById(id);
        if (category == null) {
            return false;    
        }
        repo.delete(category);
        return true;

       } catch (Exception e) {
        // TODO: handle exception
        throw new RuntimeException("Some thing wrong , please try again later");
       }

    }



    public Category addCatgeory(Category category){

        return repo.save(category);

    }
    
    public Category updateCategory(Category category){

        return repo.save(category);
    }


    public Category findByName(String name){
        return repo.cariBerdasarkanNama(name);
    }

    public List<Category> findByNameContaining(String name){
        return repo.findByNameContaining(name);
    }




}
