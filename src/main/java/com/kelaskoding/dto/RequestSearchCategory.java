package com.kelaskoding.dto;

import lombok.Data;

@Data
public class RequestSearchCategory {
    public String searchKey;
    
}
