package com.kelaskoding.dto;


import lombok.Data;

@Data
public class ResponseRegister {
    
    private String email;

    private String firstName;

    private String lastName;

    private int age;

}
