package com.kelaskoding.dto;

import com.kelaskoding.validator.PasswordEqualsConstrain;
import com.kelaskoding.validator.UniqueEmailConstraint;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Pattern;
import lombok.Data;

@Data
@PasswordEqualsConstrain
public class RequestRegister {

    @NotEmpty(message = "email is requeired")
    //@Email(message = "Email is not valid")
    @Pattern(regexp = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9,-]+\\.[a-zA-Z]{2,6}$", message = "email is not valid")
    @UniqueEmailConstraint
    private String email;

    @NotEmpty(message = "password is required")
    private String password;

    @NotEmpty(message = "retypePassword is required")
    private String retypePassword;

    @NotEmpty(message = "firstname is required")
    private String firstName;

  

    @NotEmpty(message = "last name is required")
    private String lastName;

    private int age;

    
}
