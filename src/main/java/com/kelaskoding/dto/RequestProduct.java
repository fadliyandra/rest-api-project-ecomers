package com.kelaskoding.dto;




import com.kelaskoding.validator.UniqueCodeConstraint;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.Data;

@Data
public class RequestProduct {
    
    /**
     *
     */
    @NotEmpty(message = "code is required")
    @Size(min = 3, max = 5, message = "code must be between 3 and 5 character")
    @Pattern(regexp = "P[0-9]+", message = "Code must start witgh P followed by numbers")
    @UniqueCodeConstraint
    private String code;

    @NotEmpty(message = "name is required")
    private String name;

   // @NotEmpty(message = "price is required")
    @NotNull(message = "price is required")
    private double price;

    @NotEmpty(message = "Description is requeired")
    private String description;

    //@NotEmpty(message = "Category is required")

    @NotNull(message = "categoryId is required")
    private long categoryId;
}
